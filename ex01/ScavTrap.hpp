#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP
# include "ClapTrap.hpp"

class	ScavTrap: public virtual ClapTrap
{
	public:
		void		attack(const std::string &target);
		void		guardGate(void);
		ScavTrap	&operator =(const ScavTrap &copy);
		~ScavTrap(void);
		ScavTrap(void);
		ScavTrap(const std::string initname);
		ScavTrap(const ScavTrap &copy);
};

#endif
