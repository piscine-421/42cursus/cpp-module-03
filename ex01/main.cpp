#include "ClapTrap.hpp"
#include "ScavTrap.hpp"

int	main(void)
{
	ClapTrap	Steven("Steven");
	ScavTrap	Garry("Garry");

	Garry.beRepaired(90);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Garry.attack("Steven");
	Steven.takeDamage(20);
	Garry.guardGate();
}
