#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP
# include <string>

class	ClapTrap
{
	public:
		void		attack(const std::string &target);
		void		beRepaired(unsigned int amount);
		~ClapTrap(void);
		ClapTrap(void);
		ClapTrap(const std::string initname);
		ClapTrap(const ClapTrap &copy);
		int			get_attack_damage(void) const;
		int			get_energy_points(void) const;
		int			get_hit_points(void) const;
		std::string	get_name(void) const;
		void		set_attack_damage(int value);
		void		set_energy_points(int value);
		void		set_hit_points(int value);
		void		set_name(std::string name);
		ClapTrap	&operator=(const ClapTrap &copy);
		void		takeDamage(unsigned int amount);
	private:
		int			attack_damage;
		int			energy_points;
		int			hit_points;
		std::string	name;
};

#endif
