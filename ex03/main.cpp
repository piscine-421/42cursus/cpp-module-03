#include "DiamondTrap.hpp"
#include <iostream>

int	main(void)
{
	DiamondTrap	Mario("Mario");

	std::cout << "name: " << Mario.get_name() << ", attack_damage: " << Mario.get_attack_damage() << ", energy_points: " << Mario.get_energy_points() << ", hit_points: " << Mario.get_hit_points() << std::endl;
	Mario.attack("Mario");
	Mario.takeDamage(Mario.get_attack_damage());
	Mario.beRepaired(10);
	Mario.guardGate();
	Mario.highFivesGuys();
	Mario.whoAmI();
	std::cout << "name: " << Mario.get_name() << ", attack_damage: " << Mario.get_attack_damage() << ", energy_points: " << Mario.get_energy_points() << ", hit_points: " << Mario.get_hit_points() << std::endl;
	DiamondTrap	Luigi(Mario);
	std::cout << "name: " << Luigi.get_name() << ", attack_damage: " << Luigi.get_attack_damage() << ", energy_points: " << Luigi.get_energy_points() << ", hit_points: " << Luigi.get_hit_points() << std::endl;
	DiamondTrap	Wario = Luigi;
	std::cout << "name: " << Wario.get_name() << ", attack_damage: " << Wario.get_attack_damage() << ", energy_points: " << Wario.get_energy_points() << ", hit_points: " << Wario.get_hit_points() << std::endl;
}
