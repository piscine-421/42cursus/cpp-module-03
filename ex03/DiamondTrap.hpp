#ifndef DIAMONDTRAP_HPP
# define DIAMONDTRAP_HPP
# include "FragTrap.hpp"
# include "ScavTrap.hpp"

class	DiamondTrap: public ScavTrap, public FragTrap
{
	public:
		void		attack(const std::string &target);
		~DiamondTrap(void);
		DiamondTrap(void);
		DiamondTrap(const std::string initname);
		DiamondTrap(const DiamondTrap &copy);
		std::string	get_name(void) const;
		void		set_name(std::string name);
		DiamondTrap	&operator=(const DiamondTrap &copy);
		void		whoAmI(void);
	private:
		std::string	name;
};

#endif
