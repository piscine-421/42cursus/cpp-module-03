#include "ScavTrap.hpp"
#include <iostream>

void		ScavTrap::attack(const std::string &target)
{
	if (!get_energy_points())
	{
		std::cout << "ScavTrap " << get_name() << " is out of energy and unable to attack!" << std::endl;
		return ;
	}
	else if (!get_hit_points())
	{
		std::cout << "ScavTrap " << get_name() << " is out of hit points and unable to attack!" << std::endl;
		return ;
	}
	set_energy_points(get_energy_points() - 1);
	std::cout << "ScavTrap " << get_name() << " attacks " << target << ", causing " << get_attack_damage() << " points of damage!" << std::endl;
}

void	ScavTrap::guardGate()
{
	if (!get_energy_points())
	{
		std::cout << "ScavTrap " << get_name() << " is out of energy and cannot enter Gate keeper mode!" << std::endl;
		return ;
	}
	else if (!get_hit_points())
	{
		std::cout << "ScavTrap " << get_name() << " is out of hit points and cannot enter Gate keeper mode!" << std::endl;
		return ;
	}
	std::cout << "ScavTrap " << get_name() << " has entered Gate keeper mode!" << std::endl;
}

ScavTrap	&ScavTrap::operator =(const ScavTrap &copy)
{
	set_attack_damage(copy.get_attack_damage());
	set_energy_points(copy.get_energy_points());
	set_hit_points(copy.get_hit_points());
	set_name(copy.get_name());
	std::cout << "ScavTrap " << get_name() << " has been created from ScavTrap " << copy.get_name() << "!" << std::endl;
	return (*this);
}

ScavTrap::~ScavTrap(void)
{
	std::cout << "ScavTrap " << get_name() << " has been destroyed!" << std::endl;
}

ScavTrap::ScavTrap(void)
{
	set_attack_damage(20);
	set_energy_points(50);
	set_hit_points(50);
	set_name("default");
	std::cout << "ScavTrap has been created!" << std::endl;
}

ScavTrap::ScavTrap(const std::string initname)
{
	set_attack_damage(20);
	set_energy_points(50);
	set_hit_points(50);
	set_name(initname);
	std::cout << "ScavTrap " << get_name() << " has been created!" << std::endl;
}

ScavTrap::ScavTrap(const ScavTrap &copy)
{
	*this = copy;
}
