#include "DiamondTrap.hpp"
#include <iostream>

void		DiamondTrap::attack(const std::string &target)
{
	ScavTrap::attack(target);
}

DiamondTrap::~DiamondTrap(void)
{
	std::cout << "DiamondTrap " << DiamondTrap::name << " has been destroyed!" << std::endl;
}

DiamondTrap::DiamondTrap(void)
{
	DiamondTrap::set_attack_damage(FragTrap::get_attack_damage());
	DiamondTrap::set_energy_points(ScavTrap::get_energy_points());
	DiamondTrap::set_hit_points(FragTrap::get_hit_points());
	DiamondTrap::name = "default";
	ClapTrap::set_name(DiamondTrap::name + "_clap_name");
	std::cout << "DiamondTrap " << DiamondTrap::name << " has been created!" << std::endl;
}

DiamondTrap::DiamondTrap(std::string initname)
{
	DiamondTrap::set_attack_damage(FragTrap::get_attack_damage());
	DiamondTrap::set_energy_points(ScavTrap::get_energy_points());
	DiamondTrap::set_hit_points(FragTrap::get_hit_points());
	DiamondTrap::name = initname;
	ClapTrap::set_name(DiamondTrap::name + "_clap_name");
	std::cout << "DiamondTrap " << DiamondTrap::name << " has been created!" << std::endl;
}

DiamondTrap::DiamondTrap(const DiamondTrap &copy): ClapTrap(copy), ScavTrap(copy), FragTrap(copy)
{
	DiamondTrap::name = copy.get_name();
	std::cout << "DiamondTrap " << DiamondTrap::name << " has been created from DiamondTrap " << copy.get_name() << "!" << std::endl;
}


std::string	DiamondTrap::get_name(void) const
{
	return (DiamondTrap::name);
}

DiamondTrap	&DiamondTrap::operator=(const DiamondTrap &copy)
{
	ClapTrap::operator=(copy);
	FragTrap::operator=(copy);
	ScavTrap::operator=(copy);
	DiamondTrap::name = copy.get_name();
	std::cout << "DiamondTrap " << DiamondTrap::name << " has been created from DiamondTrap " << copy.get_name() << "!" << std::endl;
	return (*this);
}

void		DiamondTrap::set_name(std::string value)
{
	DiamondTrap::name = value;
}

void		DiamondTrap::whoAmI(void)
{
	if (!DiamondTrap::get_hit_points())
		std::cout << "DiamondTrap " << DiamondTrap::name << " is out of hit points and unable to wonder about its identity!" << std::endl;
	else
		std::cout << "Am am " << DiamondTrap::name << ", or am I " << ClapTrap::get_name() << "?" << std::endl;
}
