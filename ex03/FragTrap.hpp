#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP
# include "ClapTrap.hpp"

class	FragTrap: public virtual ClapTrap
{
	public:
		void		attack(const std::string &target);
		void		highFivesGuys(void);
		FragTrap	&operator=(const FragTrap &copy);
		~FragTrap(void);
		FragTrap(void);
		FragTrap(const std::string initname);
		FragTrap(const FragTrap &copy);
};

#endif
