#include "ClapTrap.hpp"

int	main(void)
{
	ClapTrap	Steven("Steven");
	ClapTrap	Garry("Garry");

	Garry.beRepaired(90);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
}
