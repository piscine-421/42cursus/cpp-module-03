#include "FragTrap.hpp"
#include <iostream>

void		FragTrap::attack(const std::string &target)
{
	if (!get_energy_points())
	{
		std::cout << "FragTrap " << get_name() << " is out of energy and unable to attack!" << std::endl;
		return ;
	}
	else if (!get_hit_points())
	{
		std::cout << "FragTrap " << get_name() << " is out of hit points and unable to attack!" << std::endl;
		return ;
	}
	set_energy_points(get_energy_points() - 1);
	std::cout << "FragTrap " << get_name() << " attacks " << target << ", causing " << get_attack_damage() << " points of damage!" << std::endl;
}

void	FragTrap::highFivesGuys()
{
	if (!get_energy_points())
	{
		std::cout << "FragTrap " << get_name() << " is out of energy and cannot high five!" << std::endl;
		return ;
	}
	else if (!get_hit_points())
	{
		std::cout << "FragTrap " << get_name() << " is out of hit points and cannot high five!" << std::endl;
		return ;
	}
	std::cout << "FragTrap " << get_name() << " has requested a high five!!" << std::endl;
}

FragTrap	&FragTrap::operator=(const FragTrap &copy)
{
	set_attack_damage(copy.get_attack_damage());
	set_energy_points(copy.get_energy_points());
	set_hit_points(copy.get_hit_points());
	set_name(copy.get_name());
	std::cout << "FragTrap " << get_name() << " has been created from FragTrap " << copy.get_name() << "!" << std::endl;
	return (*this);
}

FragTrap::~FragTrap(void)
{
	std::cout << "FragTrap " << get_name() << " has been destroyed!" << std::endl;
}

FragTrap::FragTrap(void)
{
	set_attack_damage(30);
	set_energy_points(100);
	set_hit_points(100);
	set_name("default");
	std::cout << "FragTrap has been created!" << std::endl;
}

FragTrap::FragTrap(const std::string initname)
{
	set_attack_damage(30);
	set_energy_points(100);
	set_hit_points(100);
	set_name(initname);
	std::cout << "FragTrap " << get_name() << " has been created!" << std::endl;
}

FragTrap::FragTrap(const FragTrap &copy)
{
	*this = copy;
}
