#include "ClapTrap.hpp"
#include <iostream>

void		ClapTrap::attack(const std::string &target)
{
	if (!energy_points)
	{
		std::cout << "ClapTrap " << name << " is out of energy and unable to attack!" << std::endl;
		return ;
	}
	else if (!hit_points)
	{
		std::cout << "ClapTrap " << name << " is out of hit points and unable to attack!" << std::endl;
		return ;
	}
	energy_points--;
	std::cout << "ClapTrap " << name << " attacks " << target << ", causing " << attack_damage << " points of damage!" << std::endl;
}

void		ClapTrap::beRepaired(unsigned int amount)
{
	if (!energy_points)
	{
		std::cout << "ClapTrap " << name << " is out of energy and unable to repair itself!" << std::endl;
		return ;
	}
	else if (!hit_points)
	{
		std::cout << "ClapTrap " << name << " is out of hit points and unable to repair itself!" << std::endl;
		return ;
	}
	energy_points--;
	hit_points += amount;
	std::cout << "ClapTrap " << name << " repairs itself for " << amount << " hit points, raising its health to " << hit_points << " hit points!" << std::endl;
}

ClapTrap::~ClapTrap(void)
{
	std::cout << "ClapTrap " << name << " has been destroyed!" << std::endl;
}

ClapTrap::ClapTrap(void)
{
	attack_damage = 0;
	energy_points = 10;
	hit_points = 10;
	name = "default";
	std::cout << "ClapTrap has been created!" << std::endl;
}

ClapTrap::ClapTrap(const std::string initname)
{
	attack_damage = 0;
	energy_points = 10;
	hit_points = 10;
	name = initname;
	std::cout << "ClapTrap " << name << " has been created!" << std::endl;
}

ClapTrap::ClapTrap(const ClapTrap &copy)
{
	*this = copy;
}

int			ClapTrap::get_attack_damage(void) const
{
	return (attack_damage);
}

int			ClapTrap::get_energy_points(void) const
{
	return (energy_points);
}

int			ClapTrap::get_hit_points(void) const
{
	return (hit_points);
}

std::string	ClapTrap::get_name(void) const
{
	return (name);
}

void		ClapTrap::set_attack_damage(int value)
{
	attack_damage = value;
}

void		ClapTrap::set_energy_points(int value)
{
	energy_points = value;
}

void		ClapTrap::set_hit_points(int value)
{
	hit_points = value;
}

void		ClapTrap::set_name(std::string value)
{
	name = value;
}

ClapTrap	&ClapTrap::operator=(const ClapTrap &copy)
{
	attack_damage = copy.attack_damage;
	energy_points = copy.energy_points;
	hit_points = copy.hit_points;
	name = copy.name;
	std::cout << "ClapTrap " << name << " has been created from ClapTrap " << copy.name << "!" << std::endl;
	return (*this);
}

void		ClapTrap::takeDamage(unsigned int amount)
{
	if (!hit_points)
	{
		std::cout << "ClapTrap " << name << " gets hit for " << amount << " damage, but is already dead!" << std::endl;
		return ;
	}
	hit_points -= amount;
	if (hit_points <= 0)
	{
		hit_points = 0;
		std::cout << "ClapTrap " << name << " gets hit, suffers " << amount << " damage and dies!" << std::endl;
	}
	else
		std::cout << "ClapTrap " << name << " gets hit and suffers " << amount << " damage, lowering its health to " << hit_points << " hit points!" << std::endl;
}
