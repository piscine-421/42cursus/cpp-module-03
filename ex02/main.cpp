#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int	main(void)
{
	ClapTrap	Steven("Steven");
	ScavTrap	Garry("Garry");
	FragTrap	Mark("Mark");
	FragTrap	Jerry("Jerry");

	Mark.attack("Jerry");
	Jerry.takeDamage(30);
	Garry.beRepaired(90);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Steven.attack("Garry");
	Garry.takeDamage(10);
	Garry.attack("Steven");
	Steven.takeDamage(20);
	Mark.highFivesGuys();
	Garry.guardGate();
}
